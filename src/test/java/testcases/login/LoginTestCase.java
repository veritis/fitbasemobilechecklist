package testcases.login;

import org.apache.log4j.PropertyConfigurator;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.fitbasemobiledailychecklist.qa.helpers.sample;

import fitbasemobiledailychecklist.qa.actions.LoginAction;
import fitbasemobiledailychecklist.qa.helpers.BaseClass;
import fitbasemobiledailychecklist.qa.testdata.AppDataProvider;
import fitbasemobiledailychecklist.qa.utility.Log;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;

public class LoginTestCase {
	 
	static AndroidDriver<AndroidElement> driver;

@BeforeClass
public void fitbaseAppLaunch() throws Exception{
	PropertyConfigurator.configure("log4j.properties");	
	driver = sample.LanuchFitbaseMobileApp();
	Log.info("Fitbase App Launched Successfully");}


@Test(priority = 0, dataProvider = "FitbaseLogin" , dataProviderClass = AppDataProvider.class)
public void verifyFitbaseAppValidLogin(String Username ,String Password) throws Exception{
	LoginAction.fitbaseMobileAppLoginTestCase(driver, Username, Password);
//	LoginAction.fitbaseMobileAppLogoutTestCase(driver);
	Log.info("Successfully validated Fitbase Login functionality with valid Credentials");}

//@AfterClass 
//public void FitbaseAppCloseApp() throws Exception{
//	driver.quit(); 
//	Log.info("Fitbase App closed succesfully");}
}

