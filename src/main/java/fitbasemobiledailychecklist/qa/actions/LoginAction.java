package fitbasemobiledailychecklist.qa.actions;

import org.testng.Assert;

import fitbasemobiledailychecklist.qa.pages.LoginPage;
import fitbasemobiledailychecklist.qa.utility.Log;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;

public class LoginAction {
	
	// Check Fitbase Mobile App Home Page
	
public static void fitbaseMobileAppHomePageTestCase(AndroidDriver<AndroidElement> driver) throws Exception {
	LoginPage login = new LoginPage(driver);
	
	Actions.verifyMobileLandingPage(driver);
	Log.info("Verified Fitbase Mobile App Home page");

	Assert.assertTrue(login.FitbaseMobileHeaderIconText.isDisplayed());
	Log.info("Fitbase App Header Icon Text Verfied");

	Assert.assertTrue(login.FitbaseMobileCaptionText.isDisplayed());
	Log.info("Fitbase App Caption Text Verfied");
	
	Assert.assertTrue(login.FitbaseMobileFacebookButton.isDisplayed());
	Log.info("Fitbase App Facebook Button Verfied");

	Assert.assertTrue(login.FitbaseMobileGmailButton.isDisplayed());
	Log.info("Fitbase App Gmail Button Verfied");

	Assert.assertTrue(login.FitbaseMobileSignupText.isDisplayed());
	Log.info("Fitbase App Signup Text Verfied");

	Assert.assertTrue(login.FitbaseMobileLoginText.isDisplayed());
	Log.info("Fitbase App Login Text Verfied");}

// Check Fitbase Mobile Login Page

public static void fitbaseMobileLoginPageTestCase(AndroidDriver<AndroidElement> driver) throws Exception {
	LoginPage login = new LoginPage(driver);
	
	login.FitbaseAppLoginBtn.click();
	Assert.assertTrue(login.LoginHeaderText.isDisplayed());
	Log.info("Fitbase App Header Icon Text Verfied");

	Assert.assertTrue(login.LoginEmailAddressText.isDisplayed());
	Log.info("Fitbase App Login Email Address Text Verfied");

	Assert.assertTrue(login.LoginEmailAddressField.isDisplayed());
	Log.info("Fitbase App Login Email Address Field Verfied");

	Assert.assertTrue(login.LoginPasswordText.isDisplayed());
	Log.info("Fitbase App Login Password Text Verfied");
	
	Assert.assertTrue(login.LoginPasswordField.isDisplayed());
	Log.info("Fitbase App Login Password Field Verfied");
	
	Assert.assertTrue(login.LoginEyeIcon.isDisplayed());
	Log.info("Fitbase App Login Eye Icon Verfied");
	
	Assert.assertTrue(login.LoginButton.isDisplayed());
	Assert.assertFalse(login.LoginButton.isEnabled());
	Log.info("Fitbase App Login Button Verfied");
	
	Assert.assertTrue(login.LoginPageBackArrow.isDisplayed());
	Log.info("Fitbase App Login Page Back Arrow Verfied");
	
	Assert.assertTrue(login.LoginDonthaveanaccountText.isDisplayed());
	Log.info("Fitbase App Login Dont have an accountText Verfied");
	
	Assert.assertTrue(login.SignupCreateAccountLink.isDisplayed());
	Log.info("Fitbase App Signup Create Account Link Verfied");
	
	Assert.assertTrue(login.ForgotPasswordLink.isDisplayed());
	Log.info("Fitbase App Forgot Password Link Verfied");}
	
// Check Fitbase Mobile App Valid Login

public static void fitbaseMobileAppLoginTestCase(AndroidDriver<AndroidElement> driver , String Uname ,String Pswd) throws Exception {
	LoginPage login = new LoginPage(driver);
	
	login.FitbaseMobilelandingpageSkipbutton.click();
	Thread.sleep(2000);
	
	login.FitbaseAppLoginBtn.click();
	Thread.sleep(3000);
	
	login.FitbaseAppUserEmailID.sendKeys(Uname);
	Actions.waitElementByAndroidElement(driver, login.FitbaseAppUserEmailID, 2);
	Log.info("Entered Fitbase user valid username");
	//Thread.sleep(5000);
	
	login.FitbaseAppUserPassword.click();
	login.FitbaseAppUserPassword.sendKeys(Pswd);
	Actions.waitElementByAndroidElement(driver, login.FitbaseAppUserPassword, 2);
	Log.info("Entered Fitbase user valid Password");
	
	login.FitbaseLoginBtn.click();
	Thread.sleep(3000);
	Log.info("Click action performed on Login Button.");
		
	//Actions.verifyMobileDashboard(driver);
	//Log.info("Fitbase Mobile App Dashboard page Verified ");
	
	Thread.sleep(10000);	
	}

//Check Fitbase Mobile App Logout

public static void fitbaseMobileAppLogoutTestCase(AndroidDriver<AndroidElement> driver) throws Exception {
	LoginPage logout = new LoginPage(driver);
	
	logout.FitbaseMobileProfile.click();
	Log.info("Click Action Performed on profile tab");
	Thread.sleep(2000);
	
	//Actions.menuPageScrollup(driver);
	logout.FitbaseMobileOtherstab.click();
	Log.info("Click Action Performed on others tab");
	Thread.sleep(2000);
	
	Assert.assertTrue(logout.FitbaseMobilecommunitypage.isDisplayed());
	logout.FitbaseLogoutBtn.click();
	Log.info("Click Action Performed on Logout Button");
	Thread.sleep(5000);}

// Check login Password Eye Icon TestCase

public static void fitbaseMobileAppPasswordEyeIconTestCase(AndroidDriver<AndroidElement> driver, String Uname ,String Pswd) throws Exception {	
	LoginPage login = new LoginPage(driver);
	
	login.FitbaseAppLoginBtn.click();

	login.FitbaseAppUserEmailID.sendKeys(Uname);
	Actions.waitElementByAndroidElement(driver, login.FitbaseAppUserEmailID, 1);
	Log.info("Entered Fitbase user valid username");
		
	login.FitbaseAppUserPassword.sendKeys(Pswd);
	Actions.waitElementByAndroidElement(driver, login.FitbaseAppUserPassword, 1);
	Log.info("Entered Fitbase user valid Password");

	//Eye Icon Visibility by default is in disable mode 
	Assert.assertTrue(login.LoginEyeIconVisibilityOff.isDisplayed());
	
	login.LoginEyeIconVisibilityOff.click();
	Log.info("Click Action Performed on Visibility Off Password Eye Icon.");

	//Click on Eye Icon Visibility or text entered in password field will be visual 
	Assert.assertTrue(login.LoginEyeIconVisibilityOn.isDisplayed());
	login.LoginPasswordField.isDisplayed();
	Log.info("Entered password text is Visible");
	
	login.LoginEyeIconVisibilityOn.click();
	Log.info("Click Action Performed on Visibility On Password Eye Icon.");
	Log.info("Entered password text is not Visible");}

// Check "Forget Password" link

public static void fitbaseMobileAppForgotPasswordLinkTestCase(AndroidDriver<AndroidElement> driver) throws Exception {
	LoginPage login = new LoginPage(driver);
	LoginPage forgotPassword = new LoginPage(driver);
	
	login.FitbaseAppLoginBtn.click();
	Assert.assertTrue(forgotPassword.ForgotPasswordLink.isDisplayed());
	Log.info("Forgot Password Link Verfied");
		
	forgotPassword.ForgotPasswordLink.click();
	Log.info("Click Action Performed on Forgot Password Link.");
	
	Assert.assertEquals("Forgot Password", "Forgot Password");}
	//Log.info("Verfied Next Page Header Text");

//Check "Forget Password" Page

public static void fitbaseMobileAppForgotPasswordPageTestCase(AndroidDriver<AndroidElement> driver) throws Exception {
	LoginPage forgotPassword = new LoginPage(driver);
	
	Assert.assertTrue(forgotPassword.ForgotPasswordPageHeaderText.isDisplayed());
	Log.info("Forgot Password Page Header Text Verfied");
	
	Assert.assertTrue(forgotPassword.ForgotPasswordBackArrow.isDisplayed());
	Log.info("Forgot Password Back Arrow Verfied");
	
	Assert.assertTrue(forgotPassword.ForgotPasswordEmailAddressText.isDisplayed());
	Log.info("Forgot Password Email Address Text Verfied");
	
	Assert.assertTrue(forgotPassword.ForgotPasswordEmailAddressField.isDisplayed());
	Log.info("Forgot Password Email Address Field Verfied");
	
	Assert.assertTrue(forgotPassword.ForgotPasswordSendResetLinkButton.isDisplayed());
	Log.info("Forgot Password Send Reset Link Button Verfied");}

public static void forgotPasswordValidEmailTestCase(AndroidDriver<AndroidElement> driver,String Email ) throws Exception{ 
	
	LoginPage login = new LoginPage(driver);
	
	login.ForgotPasswordEmailAddressField.sendKeys(Email);
	Log.info("Entered Valid User Email Address");

	Assert.assertTrue(login.ForgotPasswordSendResetLinkButton.isEnabled());
	login.ForgotPasswordSendResetLinkButton.click();
	Log.info("Click Action Performed on Forgot Password Send Reset Link Button.");
	
	//boolean EmailAddressIsValid =false;
	
//	EmailAddressIsValid = (Action.validateEmail(Email)&& Email.length()>0 &&Email.length()<=50) ? true : false;
		
 //if(!EmailAddressIsValid){
	//Assert.assertTrue(login.ForgotPasswordMailSentConfirmation.isDisplayed());
	//Action.waitElementByWebElement(driver, login.ForgotPasswordMailSentConfirmation, 20);	
	Log.info("Verfied Forgot Password Mail Sent Confirmation Message Text");}

}

