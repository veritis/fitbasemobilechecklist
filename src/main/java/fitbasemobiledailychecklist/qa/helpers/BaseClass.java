package fitbasemobiledailychecklist.qa.helpers;

import java.net.URL;
import org.openqa.selenium.remote.DesiredCapabilities;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;

public class BaseClass {
	
		static AndroidDriver<AndroidElement> driver;
		
public static AndroidDriver<AndroidElement> LanuchFitbaseMobileApp() {
	
	try {
		 // Created object of DesiredCapabilities class.
		  DesiredCapabilities capabilities = new DesiredCapabilities();

		  // Set android deviceName desired capability. Set it Android Emulator.
		  capabilities.setCapability("deviceName", "192.168.207.101:5555");

		  // Set browserName desired capability. It's Android in our case here.
		 // capabilities.setCapability("browserName", "Android");

		  // Set android platformVersion desired capability. Set your emulator's android version.
		  capabilities.setCapability("platformVersion", "10");

		  // Set android platformName desired capability. It's Android in our case here.
		  capabilities.setCapability("platformName", "Android");
		  		  
		  // Set your application's appPackage if you are using any other app.
		  capabilities.setCapability("appPackage", "com.fitbase");

		  // Set your application's appPackage if you are using any other app.
		  capabilities.setCapability("appActivity", "com.fitbase.MainActivity");

		  // Created object of RemoteWebDriver will all set capabilities.
		  // Set appium server address and port number in URL string.
		  // It will launch fitbaseTariner Native app in emulator.
		  URL url = new URL("http://0.0.0.0:4723/wd/hub");
		  driver = new AndroidDriver<AndroidElement>(url,capabilities);
		 
		  // Can use below staements aslso,appium is parent and below are appium chailds with specific actions
		  //  driver = new AndroidDriver<MobileElement>(url,capabilities);
		  // driver = new IOSDriver<MobileElement>(url,capabilities);
		//  driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
		  Thread.sleep(7000);

	}catch(Exception e) {
		
		 System.out.println("Message is : "+e.getMessage());
		 e.printStackTrace();
	}
		  return driver;	
	}
}



