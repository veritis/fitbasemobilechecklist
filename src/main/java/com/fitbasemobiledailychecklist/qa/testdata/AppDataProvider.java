package com.fitbasemobiledailychecklist.qa.testdata;

import org.testng.annotations.DataProvider;

public class AppDataProvider {
	@DataProvider(name = "FitbaseLogin" )//New Registered User
	public static Object[][] getDataFromUserLoginDataprovider(){
	return new Object[][]{{"sunilkumar10@mailinator.com", "password"}};
}
	@DataProvider(name = "GmailLogin" )//Social Login
	public static Object[][] gmailDataprovider(){
	return new Object[][]{{"sunilkumartestingfitbase@gmail.com", "Testing@123"}};
}
	@DataProvider(name = "FacebookLogin" )//Social Login
	public static Object[][] FacebookDataprovider(){
	return new Object[][]{{"fitbase.mail1@gmail.com", "testing123$"}};}
	
	@DataProvider(name = "ForgotPasswordValidTestData")
	public static Object[][] getDatafromDataProvider(){
	return new Object[][]{{"sunilkumar10@mailinator.com"}};}
}
