package com.fitbasemobiledailychecklist.qa.actions;

import static io.appium.java_client.touch.WaitOptions.waitOptions;

import java.time.Duration;

import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.Point;
import org.openqa.selenium.interactions.touch.TouchActions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import io.appium.java_client.MobileElement;
import io.appium.java_client.TouchAction;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.touch.WaitOptions;
import io.appium.java_client.touch.offset.PointOption;

public class Actions {
public static void menuPageScroll(AndroidDriver<AndroidElement> driver) throws Exception{
		TouchActions action = new TouchActions(driver);
		action.scroll(518, 1071);}
	
public static void verifyMobileDashboard(AndroidDriver<AndroidElement> driver) {
		WebDriverWait wait = new WebDriverWait(driver,10);
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View/android.view.View/android.view.View/android.view.View/android.view.View/android.view.View[1]/android.view.View[1]/android.view.View[2]/android.view.View[2]/android.view.View")));}
	
public static void verifyMobileLandingPage(AndroidDriver<AndroidElement> driver) {
		WebDriverWait wait = new WebDriverWait(driver,15);
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View/android.view.View/android.view.View/android.view.View/android.view.View/android.view.View/android.view.View/android.view.View[3]/android.view.View[1]/android.view.View[1]/android.view.View")));}
	//wait statements
	
public static void verifyMenuIcon(AndroidDriver<AndroidElement> driver) {
		WebDriverWait wait = new WebDriverWait(driver,10);
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View/android.view.View/android.view.View/android.view.View/android.view.View/android.view.View[1]/android.view.View[1]/android.view.View[2]/android.view.View[1]/android.view.View/android.widget.Button")));}

public static void waitElementByAndroidElement(AndroidDriver<AndroidElement> driver ,MobileElement element,int numberOfSeconds) {	
		WebDriverWait wait = new WebDriverWait(driver, numberOfSeconds);
		wait.until( ExpectedConditions.visibilityOf(element));}
	
@SuppressWarnings("rawtypes")
public static void menuPageScrollup(AndroidDriver<AndroidElement> driver) {
		Dimension size = driver.manage().window().getSize();
		   
	    Double scrollHeightStart = size.getHeight() * 0.8;
	    int scrollStart = scrollHeightStart.intValue();
	     
	    Double scrollHeightEnd = size.getHeight() * 0.2;
	    int scrollEnd = scrollHeightEnd.intValue();
	                   
	    TouchAction actions = new TouchAction(driver);
	    actions.press(PointOption.point(0, scrollStart))
	    .waitAction(waitOptions(Duration.ofSeconds(2)))
	    .moveTo(PointOption.point(0, scrollEnd)).release().perform();}

@SuppressWarnings("rawtypes")
public static void menuPageScrollDown(AndroidDriver<AndroidElement> driver) {
		Dimension size = driver.manage().window().getSize();
		   
	    Double scrollHeightStart = size.getHeight() * 0.2;
	    int scrollStart = scrollHeightStart.intValue();
	     
	    Double scrollHeightEnd = size.getHeight() * 0.8;
	    int scrollEnd = scrollHeightEnd.intValue();
	                   
	    TouchAction actions = new TouchAction(driver);
	    actions.press(PointOption.point(0, scrollStart))
	    .waitAction(waitOptions(Duration.ofSeconds(2)))
	    .moveTo(PointOption.point(0, scrollEnd)).release().perform();}

//@SuppressWarnings("rawtypes")
//public static void waterTabSwipeLeft(AndroidDriver<AndroidElement> driver) throws Exception {
//
//	WaterPage water =new WaterPage(driver);
//
//    MobileElement waterTabtoSwipe = water.FitbaseAppDayTodaysRecentlyLoggedGlassSwipe; 
//    Point location = waterTabtoSwipe.getLocation();
//    Dimension screenSize = driver.manage().window().getSize();
//    
//     int startX = Math.toIntExact(Math.round(screenSize.getWidth() * 0.8));
//     int endX = 0;
//      TouchAction action = new TouchAction(driver); action
//      .press(PointOption.point(startX, location.getY()))
//      .waitAction(WaitOptions.waitOptions(Duration.ofMillis(500)))
//      .moveTo(PointOption.point(endX, location.getY())) .release();
//      driver.performTouchAction(action);
//     
//}
	}

